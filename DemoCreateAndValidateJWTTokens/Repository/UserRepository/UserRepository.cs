﻿using DemoCreateAndValidateJWTTokens.Libraries;
using DemoCreateAndValidateJWTTokens.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoCreateAndValidateJWTTokens.Repository.UserRepositories
{
    public class UserRepository : IUserRepository
    {
        private readonly List<UserModel> users = new List<UserModel>();
        public UserRepository()
        {
            users.Add(new UserModel { Id = new Guid("98596aa6-5347-41c7-a79c-29f1524a9f9f"), UserName = "mystogancn", Password = "cuongnguyen".Md5(), Email = "nqcuong720@gmail.com" });
            users.Add(new UserModel { Id = new Guid("a135ade9-867b-4fbb-a8d5-fab6412519bd"), UserName = "mystogancn1", Password = "cuongnguyen1".Md5(), Email = "nqcuong720@gmail.com" });
        }
        public UserModel GetUser(LoginRequest request)
        {
            var result = users.Where(x => x.UserName == request.UserName && x.Password == request.Password.Md5()).SingleOrDefault();
            return result;
        }

        public UserModel GetUserByUserName(string userName)
        {
            return users.Where(x => x.UserName == userName).SingleOrDefault();
        }
    }
}
