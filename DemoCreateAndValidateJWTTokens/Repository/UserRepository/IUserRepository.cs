﻿using DemoCreateAndValidateJWTTokens.Models;
using System;
using System.Threading.Tasks;

namespace DemoCreateAndValidateJWTTokens.Repository.UserRepositories
{
    public interface IUserRepository
    {
        UserModel GetUser(LoginRequest request);

        UserModel GetUserByUserName(string userName);
    }
}
