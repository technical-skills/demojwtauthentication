﻿using DemoCreateAndValidateJWTTokens.Models;
using System.Collections.Generic;

namespace DemoCreateAndValidateJWTTokens.Repository.ProductRepository
{
    public class ProductRepository : IProductRepository
    {
        public List<ProductViewModel> GetAllProduct()
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            products.Add(new ProductViewModel { Id = 1, ProductName = "Computer", Price = 4500 });
            products.Add(new ProductViewModel { Id = 2, ProductName = "Mouse", Price = 500 });
            products.Add(new ProductViewModel { Id = 3, ProductName = "HDD", Price = 1500 });
            return products;
        }
    }
}
