﻿using DemoCreateAndValidateJWTTokens.Models;
using System.Collections.Generic;

namespace DemoCreateAndValidateJWTTokens.Repository.ProductRepository
{
    public interface IProductRepository
    {
        List<ProductViewModel> GetAllProduct();
    }
}
