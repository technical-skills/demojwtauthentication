﻿namespace DemoCreateAndValidateJWTTokens.Models
{
    public class LoginResponse
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }
    }
}
