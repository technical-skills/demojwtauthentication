﻿using DemoCreateAndValidateJWTTokens.Common;
using DemoCreateAndValidateJWTTokens.Repository.UserRepositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DemoCreateAndValidateJWTTokens.Middlewares
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class AuthMiddleware
    {
        private readonly RequestDelegate _next;

        private readonly IUserRepository _userRepository;

        public AuthMiddleware(RequestDelegate next, IUserRepository userRepository)
        {
            _next = next;
            _userRepository = userRepository;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            //check nếu tồn tại token thì check account.
            //không có token thì đi vào ApiAuthorizeAttribute xem action này có bypass author ko . Nếu không thì return.
            //OK pass vào trong để ApiAuthorizeAttribute check. Nếu tồn tại username => by pass call action . Không thì return.
            var token = httpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            if (token != null)
            {
                string userName = AttachUserByToken(httpContext,token);
                if(VerifyAccount(userName))
                {
                    await _next(httpContext);
                    return;
                }
                httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }
            await _next(httpContext);
        }

        private string AttachUserByToken(HttpContext httpContext,string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(DemoJWTConfig.SerectKey);
            tokenHandler.ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                ClockSkew = TimeSpan.Zero
            }, out SecurityToken validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;
            var userName = jwtToken.Claims.First(x => x.Type == "UserName").Value;
            httpContext.Items["UserName"] = userName;
            return userName;
        }

        private bool VerifyAccount(string userName)
        {
            var account = _userRepository.GetUserByUserName(userName);
            if (account != null)
                return true;
            else
                return false;
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class AuthMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthMiddleware>();
        }
    }
}
