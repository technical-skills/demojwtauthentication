﻿using DemoCreateAndValidateJWTTokens.Libraries;
using DemoCreateAndValidateJWTTokens.Models;
using DemoCreateAndValidateJWTTokens.Repository.UserRepositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DemoCreateAndValidateJWTTokens.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    [ApiAuthorizeAttribute]
    public class AuthenticationsController : Controller
    {
        private readonly IUserRepository _userRepository;

        public AuthenticationsController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Login([FromBody] LoginRequest loginRequest)
        {
            LoginResponse result = new LoginResponse();
            if (loginRequest == null) 
                return BadRequest();
            else
            {
                var userExist = _userRepository.GetUser(loginRequest);
                if (userExist == null)
                    return NotFound();
                else
                {
                    string token = LibrariesModel.GenerateJwtToken(userExist.UserName);
                    result.UserName = loginRequest.UserName;
                    result.Email = loginRequest.Email;
                    result.Token = token;
                    return Ok(result);
                }    
            }    
        }
    }
}
