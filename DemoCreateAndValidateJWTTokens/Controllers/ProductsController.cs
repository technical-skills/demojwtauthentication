﻿using DemoCreateAndValidateJWTTokens.Repository.ProductRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DemoCreateAndValidateJWTTokens.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiAuthorizeAttribute]
    public class ProductsController : Controller
    {
        private readonly IProductRepository _productRepository;

        public ProductsController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet]
        public IActionResult GetAllProduct()
        {
            var result = _productRepository.GetAllProduct();
            return Ok(result);
        }
    }
}
