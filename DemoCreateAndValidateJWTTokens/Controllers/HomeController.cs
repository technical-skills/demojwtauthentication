﻿using DemoCreateAndValidateJWTTokens.Repository.ProductRepository;
using Microsoft.AspNetCore.Mvc;

namespace DemoCreateAndValidateJWTTokens.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductRepository _productRepository;

        public HomeController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet]
        public IActionResult GetAllProduct()
        {
            var result = _productRepository.GetAllProduct();
            return Ok(result);
        }
    }
}
