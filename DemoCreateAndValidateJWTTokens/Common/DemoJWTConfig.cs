﻿namespace DemoCreateAndValidateJWTTokens.Common
{
    public class DemoJWTConfig
    {
        public static string SerectKey { get; set; }

        public static int Expires { get; set; }

        public static string[] AnonymousPaths { get; set; }
    }
}
