﻿using Microsoft.Extensions.Configuration;

namespace DemoCreateAndValidateJWTTokens.Common
{
    public class AppSettingHelper
    {
        public static void LoadConfig(IConfiguration configuration)
        {
            var sectionAppSettings = configuration.GetSection("AppSettings");

            DemoJWTConfig.SerectKey = sectionAppSettings.GetValue<string>("Secret");
            DemoJWTConfig.Expires = sectionAppSettings.GetValue<int>("ExpiresDate");

            var sectionAuthSettings = configuration.GetSection("AuthSettings:AnonymousPaths");
            DemoJWTConfig.AnonymousPaths = sectionAuthSettings.Get<string[]>();
        }
    }
}
